================
 Django-Backups
================

This is a Django application that provides facilities for systematic backups
and restores of database contents, in a database-independent way. Backups made
on one platform (say, a production server using PostgreSQL) can be restored on
another platform (say, a development machine using SQLite). This is mostly a
wrapper around the standard commands ``dumpdata``, ``loaddata`` and ``flush``.

Typical use consists in calling the ``backup`` command from a cron job. The
application only stores backups locally, off-site backups should be handled
independently.


Installation
============

You can install the application using the standard Python procedure:

    python setup.py install

To use the application in your Django project, add ``backups`` to the
``INSTALLED_APPS`` variable in your project's settings, and add required
settings (see below).


Management commands
===================

The application provides no models, views or templates, only the following
management commands:

``backup [SUFFIX]``
  Store a backup, in the configured directory, using a formatted name like
  ``dump-YYYY-MM-DD.SUFFIX.json`` by default. The suffix part is optional. In
  the backup directory, a symbolic link named ``last.json`` (by default) is
  set to point to the last backup.

``restore [FILE]``
  Reset the database and loads the backup from the specified ``FILE``. By
  default, this uses the last backup, as defined by the ``last.json`` symbolic
  link.


Settings
========

The storage of backups is controlled by the following settings variables:

``BACKUPS_DIRECTORY``
  The directory where backups will be stored. No default value is provided, it
  must be defined in the project settings.

``BACKUPS_FORMAT``
  The format for storing the backups. Any serializer known by Django can be
  used, by default ``json`` is used.

``BACKUPS_INDENT``
  The indent level to use in the dump files. For a format like ``json``,
  indenting also introduces line breaks, which makes the output usable with
  ``diff`` or version control tools.

``BACKUPS_NAME_TEMPLATE``
  The format of the file names produced by the ``backup`` command, not
  including the format-specific extension. It uses standard ``strftime``
  formatting, using the date and time of the backup. By default, it is
  ``dump-%Y-%m-%d``.

``BACKUPS_NAME_LAST``
  The name of the symbolic link that is made to point to the last backup, not
  including the extension. By default, it is ``last``.
