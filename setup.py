from distutils.core import setup

setup(
    name='django-backups',
    version='0.1',
    author=u'Emmanuel Beffara',
    author_email='manu@beffara.org',
    packages=[
        'backups',
        'backups.management',
        'backups.management.commands',
        ],
    license='BSD',
    description='Database-independent backups for Django projects.',
)
