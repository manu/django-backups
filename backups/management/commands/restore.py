from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.db import models

from backups.settings import *

import os

class Command (BaseCommand):
	args = "[backup]"
	help = u"Restore a backup of the database."

	def handle (self, *args, **kwargs):
		if len(args) > 0:
			path = args[0]
		else:
			path = os.path.join(
				BACKUPS_DIRECTORY,
				BACKUPS_NAME_LAST + '.' + BACKUPS_FORMAT)

		if not os.access(path, os.R_OK):
			raise CommandError("Cannot read the backup file.")

		call_command("flush", interactive=False)
		for model in models.get_models():
			for instance in model.objects.all():
				instance.delete()
		if 'south' in settings.INSTALLED_APPS:
			call_command("migrate", fake=True)
		call_command("loaddata", path)
