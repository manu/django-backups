import datetime
import os

from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command

from backups.settings import *

class Command (BaseCommand):
	help = u"Store a backup of the database."

	def handle (self, *args, **kwargs):
		name = datetime.datetime.now().strftime(BACKUPS_NAME_TEMPLATE)
		if len(args) > 0:
			name += '.' + args[0]
		name += '.' + BACKUPS_FORMAT

		with open(os.path.join(BACKUPS_DIRECTORY, name) , 'w') as out:
			call_command('dumpdata',
				format = BACKUPS_FORMAT,
				indent = BACKUPS_INDENT,
				stdout = out,
			)

		last = os.path.join(BACKUPS_DIRECTORY,
				BACKUPS_NAME_LAST + '.' + BACKUPS_FORMAT)
		try:
			os.unlink(last)
		except OSError:
			pass
		os.symlink(name, last)
