from django.conf import settings

BACKUPS_DIRECTORY = getattr(settings,
	'BACKUPS_DIRECTORY')

BACKUPS_FORMAT = getattr(settings,
	'BACKUPS_FORMAT', 'json')

BACKUPS_INDENT = getattr(settings,
	'BACKUPS_INDENT', 2)

BACKUPS_NAME_TEMPLATE= getattr(settings,
	'BACKUPS_NAME_TEMPLATE', 'dump-%Y-%m-%d')

BACKUPS_NAME_LAST = getattr(settings,
	'BACKUPS_NAME_LAST', 'last')
